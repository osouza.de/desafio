@extends('layouts.app')

@section('title', 'Lista')

@section('lead')
Lista contendo todas as informações cadastradas.
@endsection

@section('content')
  <div class="row justify-content-md-center">



    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>Sobrenome</th>
        <th>Email</th>
        <th>Telefone</th>
        <th>País</th>
        <th>Estado</th>
        <th>CEP</th>
        <th>Cadastrado em</th>
        <th>Editar</th>
      </tr>
    </thead>
    <tbody>

        @foreach($infos as $info)

          @php
            $date=date('H:i:s d/m/Y e', strtotime($info['created_at']));
          @endphp
            <tr>
                <td>{{ $info['id'] }}</td>
                <td>{{ $info['name'] }}</td>
                <td>{{ $info['lastname'] }}</td>
                <td>{{ $info['email'] }}</td>
                <td>{{ $info['phone'] }}</td>
                <td>{{ $info['country'] }}</td>
                <td>{{ $info['state'] }}</td>
                <td>{{ $info['zip'] }}</td>
                <td>{{ $date }}</td>
                <td>
                    <a href="/info/{{ $info['id'] }}/edit" class="btn btn-light btn-sm active" role="button" aria-pressed="true">
                        <span class="oi oi-pencil" aria-hidden="true"></span>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>

  </div>
@endsection