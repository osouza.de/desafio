@extends('layouts.app')

@section('title', 'Lista')

@section('lead')
Lista contendo todas as informações cadastradas.
@endsection

@section('content')
<div class="container">

      <div class="row justify-content-md-center">

        @if (\Session::has('success'))
          <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
          </div><br />
         @endif
 
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Informações pessoais</h4>
          <form class="needs-validation" novalidate method="post" action="{{url('info')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Nome</label>
                <input type="text" class="form-control" name="name" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Você deve informar um nome válido.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastname">Sobrenome</label>
                <input type="text" class="form-control" name="lastname" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Um sobrenome válido é obrigatório.
                </div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="nome@gmail.com">
                  <div class="invalid-feedback">
                    Insira um e-mail válido, por favor.
                  </div>
                </div>

                <div class="col-md-6 mb-3">
                  <label for="phone">Telefone</label>
                  <input type="tel" class="form-control" name="phone">
                  <div class="invalid-feedback">
                    Insira um telefone válido, por favor.
                  </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">País</label>
                <select class="custom-select d-block w-100" name="country" required>
                  <option value="br">Brasil</option>
                </select>
                <div class="invalid-feedback">
                  Por favor, selecione um país válido.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">Estado</label>
                <select class="custom-select d-block w-100" name="state" required>
                  <option value="">Escolha...</option>
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="GO">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS" selected>Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                </select>
                <div class="invalid-feedback">
                  Informe um estado válido.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">CEP</label>
                <input type="text" class="form-control" name="zip" placeholder="" required>
                <div class="invalid-feedback">
                  CEP é obrigatório.
                </div>
              </div>
            </div>

            <hr class="mb-4">

            <button class="btn btn-primary btn-lg btn-block" type="submit">Salvar</button>

            @if ($errors->any())
            <hr class="mb-4">
            <div class="col-md-8">

                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

            </div>
            @endif

          </form>
        </div>
      </div>
@endsection