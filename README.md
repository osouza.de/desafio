# Desafio!

## Iniciar

A fim de rodar o projeto, execute os passos abaixo:

1. Clone o repositório (descubra haha)

2. A partir do terminal, navegue até o diretório do projeto e execute:

```bash
cd docker
docker-compose up -d
```

O comando acima criará os contâiners e imagens necessárias. Dependendo da tua conexão e disk IO cap, pode levar alguns minutos.

2. Execute os comandos abaixo dentro do container __desafio-app__  para preparar o ambiente

(para "entrar" no container: docker exec -it desafio-app bash (no caso do windows, adicionar winpty antes do docker)

```bash
bash -c "
    cd /var/www                     &&
    echo 'Installing things'        &&
    composer install                &&
    npm install                     &&
    echo 'Compiling frontend stuff' &&
    npm run prod                    && 
    echo 'Running artisan stuff'    &&
    cp .env.example .env            &&
    php artisan key:generate        &&
    php artisan migrate             &&
    php artisan db:seed             &&
    echo 'All done, lil friend o mine\!'"
```

3. Caso o frontend apresente problemas, execute os seguintes comandos a partir da instancia __desafio-app__

```bash
su apache
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 775 {} \;
chgrp -R apache storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
rm -rf node_modules vendor
```
